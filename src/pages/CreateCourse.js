import { useState, useEffect, useContext } from 'react'
import { Container, Row, Col, Form, Button } from 'react-bootstrap'
import { useNavigate } from 'react-router-dom'

import UserContext from './../UserContext'

const token = localStorage.getItem('token')

export default function CreateCourse(){


	// update the user state using context

	// add the course by getting the user input and send it as a client request via fetch
	// send an alert once added course succesfully
	// navigate back to courses dashboard
	// course newly added must show in the dashboard at the very end of the courses

	const [courseName, setCourseName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState(0)

	const navigate = useNavigate()

	const { dispatch } = useContext(UserContext)


	useEffect(() => {
		if(token !== null){

			dispatch({type: "USER", payload: true})
		}
	}, [])


	const handleSubmit = (e) => {
		e.preventDefault()

		fetch('http://localhost:3007/api/courses/create',{
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				courseName: courseName,
				description: description,
				price: price
			})
		})
		.then(response => response.json())
		.then(response => {
			console.log(response)
			if(response){

				alert('Course successfully created!')

				navigate('/courses')
			}
		})
	}

	return(
		<Container className="container m-5">
		 	<h1 className="text-center">Add Course</h1>
			<Form onSubmit={ (e) => handleSubmit(e) }>
				<Row>
					<Col xs={12} md={8}>
						<Form.Group className="mb-3">
					    	<Form.Control
					    		placeholder="Course Name"
					    		type="text" 
					    		value={courseName}
					    		onChange={ (e) => setCourseName(e.target.value) }
					    		
					    	/>
						</Form.Group>
					</Col>
					<Col xs={12}  md={4}>
						<Form.Group className="mb-3">
					    	<Form.Control
					    		placeholder="Course Price"
					    		type="number" 
					    		value={price}
					    		onChange={ (e) => setPrice(e.target.value) }
					    	/>
						</Form.Group>
					</Col>
				</Row>
				<Row>
					<Col xs={12}  md={12}>
						<Form.Group className="mb-3">
					    	<Form.Control
					    		placeholder="Course Description"
					    		type="text" 
					    		value={description}
					    		onChange={ (e) => setDescription(e.target.value) }
					    		
					    	/>
						</Form.Group>
					</Col>
				</Row>
				<Button type="submit" className="btn btn-info btn-block">Add Course</Button>
			</Form>
		</Container>
	)
}